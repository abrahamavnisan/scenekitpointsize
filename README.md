# How to call glPointSize() (or the SceneKit equivalent) when making custom geometries using SceneKit and SCNGeometryPrimitiveTypePoint #

I'm writing an app that renders a pointcloud in SceneKit using a custom geometry.  [This post][1] was super helpful in getting me there (though I translated this to Objective-C), as was David Rönnqvist's book [3D Graphics with SceneKit][2] (see chapter on custom geometries).  The code, demonstrated here, works fine, but I'd like to make the points render at a larger point size - at the moment the points are super tiny. 

According to the [OpenGL docs][3], you can do this by calling `glPointSize()` (though evidently [when you call][4] this is important). From what I understand, SceneKit is built on top of OpenGL so I'm hoping there is a way to access this function or do the equivalent using SceneKit.  I've created this repo so I can link to it from my [SO question][5].  Any suggestions would be much appreciated!

  [1]: http://stackoverflow.com/questions/32712268/how-to-use-scenekit-to-display-a-colorful-point-cloud-using-custom-scngeometry/38252032#38252032
  [2]: http://scenekitbook.com/
  [3]: https://www.opengl.org/sdk/docs/man2/xhtml/glPointSize.xml
  [4]: https://www.opengl.org/discussion_boards/showthread.php/168004-How-to-define-Point-Size-in-openGL
  [5]: http://stackoverflow.com/questions/38257339/how-to-call-glpointsize-or-the-scenekit-equivalent-when-making-custom-geomet