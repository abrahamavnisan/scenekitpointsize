//
//  main.m
//  SceneKitPointSize
//
//  Created by Abraham Avnisan on 7/7/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
