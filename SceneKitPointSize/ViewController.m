//
//  ViewController.m
//  SceneKitPointSize
//
//  Created by Abraham Avnisan on 7/7/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

typedef struct PointcloudVertex
{
    float x, y, z, r, g, b;
} PointcloudVertex;

#import "ViewController.h"
@import SceneKit;

@interface ViewController ()

@property (weak, nonatomic) IBOutlet SCNView *myView;


@end

@implementation ViewController

- (void)setupSceneView
{
    // create an empty scene
    SCNScene *scene = [SCNScene scene];
    self.myView.scene = scene;
    
    // create a camera, position it to point at 0,0,0
    // and add it to the scene (thanks David Rönnqvist http://scenekitbook.com/ )
    SCNNode *cameraNode = [SCNNode node];
    cameraNode.camera   = [SCNCamera camera];
    cameraNode.position = SCNVector3Make(0, 12, 30);
    cameraNode.rotation = SCNVector4Make(1, 0, 0, -sin(12.0/30.0));
    
    [scene.rootNode addChildNode:cameraNode];

}
- (void)addCustomGeometry
{
    // set the number of points
    NSUInteger numPoints = 10000;
    
    // set the max distance points
    int randomPosUL = 2;
    int scaleFactor = 10000; // because I want decimal points
                             // but am getting random values using arc4random_uniform
    
    PointcloudVertex pointcloudVertices[numPoints];
    
    for (NSUInteger i = 0; i < numPoints; i++) {
        
        PointcloudVertex vertex;
        
        float x = (float)(arc4random_uniform(randomPosUL * 2 * scaleFactor));
        float y = (float)(arc4random_uniform(randomPosUL * 2 * scaleFactor));
        float z = (float)(arc4random_uniform(randomPosUL * 2 * scaleFactor));
        
        vertex.x = (x - randomPosUL * scaleFactor) / scaleFactor;
        vertex.y = (y - randomPosUL * scaleFactor) / scaleFactor;
        vertex.z = (z - randomPosUL * scaleFactor) / scaleFactor;
        
        vertex.r = arc4random_uniform(255) / 255.0;
        vertex.g = arc4random_uniform(255) / 255.0;
        vertex.b = arc4random_uniform(255) / 255.0;
        
        pointcloudVertices[i] = vertex;
        
        //        NSLog(@"adding vertex #%lu with position - x: %.3f y: %.3f z: %.3f | color - r:%.3f g: %.3f b: %.3f",
        //              (long unsigned)i,
        //              vertex.x,
        //              vertex.y,
        //              vertex.z,
        //              vertex.r,
        //              vertex.g,
        //              vertex.b);
    }
    
    // convert array to point cloud data (position and color)
    NSData *pointcloudData = [NSData dataWithBytes:&pointcloudVertices length:sizeof(pointcloudVertices)];
    
    // create vertex source
    SCNGeometrySource *vertexSource = [SCNGeometrySource geometrySourceWithData:pointcloudData
                                                                       semantic:SCNGeometrySourceSemanticVertex
                                                                    vectorCount:numPoints
                                                                floatComponents:YES
                                                            componentsPerVector:3
                                                              bytesPerComponent:sizeof(float)
                                                                     dataOffset:0
                                                                     dataStride:sizeof(PointcloudVertex)];
    
    // create color source
    SCNGeometrySource *colorSource = [SCNGeometrySource geometrySourceWithData:pointcloudData
                                                                      semantic:SCNGeometrySourceSemanticColor
                                                                   vectorCount:numPoints
                                                               floatComponents:YES
                                                           componentsPerVector:3
                                                             bytesPerComponent:sizeof(float)
                                                                    dataOffset:sizeof(float) * 3
                                                                    dataStride:sizeof(PointcloudVertex)];
    
    // create element
    SCNGeometryElement *element = [SCNGeometryElement geometryElementWithData:nil
                                                                primitiveType:SCNGeometryPrimitiveTypePoint
                                                               primitiveCount:numPoints
                                                                bytesPerIndex:sizeof(int)];
    
    // create geometry
    SCNGeometry *pointcloudGeometry = [SCNGeometry geometryWithSources:@[ vertexSource, colorSource ] elements:@[ element]];
    
    // add pointcloud to scene
    SCNNode *pointcloudNode = [SCNNode nodeWithGeometry:pointcloudGeometry];
    [self.myView.scene.rootNode addChildNode:pointcloudNode];


}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setupSceneView];
    [self addCustomGeometry];
}

@end
